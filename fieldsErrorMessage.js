import get from 'lodash/get';

export function fieldsErrorMessage(errors = {}, validator) {
    const errorKeys = Object.keys(errors);

    if (!errorKeys.length) {
        return;
    }

    errorKeys.forEach((errorKey) => {
        const error = get(validator, errorKey);

        if (error) {
            error.$reset();
            error.serverError = Array.isArray(errors[errorKey])
                ? errors[errorKey][0]
                : errors[errorKey];
        }
    });
}
