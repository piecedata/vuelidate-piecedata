import { req, withParams } from 'vuelidate/lib/validators/common';

export function tel(format) {
    return withParams({
        type: 'tel',
        format,
    }, (value) => {
        const telRegex = /^(\+1)?[-.\s]?([0-9]{3})[-.\s]?([0-9]{3})[-.\s]?([0-9]{4})$/;
        return !req(value) || telRegex.test(value);
    });
}

export function includes(arr) {
    return withParams({
        type: 'includes',
        arr,
    }, value => arr.includes(value));
}

export function size(max, size = 'kilobytes') {
    return withParams({
        type: 'size',
        max,
        size,
    }, value => !req(value) || +max >= +value);
}
