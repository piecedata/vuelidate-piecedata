const messages = {
    alpha: field => `The ${field} field must contain alphabetic characters.`,
    alphaNum: field => `The ${field} field must contain alpha-numeric characters.`,
    between: (field, { min, max }) => `The ${field} field must be between ${min} and ${max}.`,
    decimal: field => `The ${field} field must be a decimal number.`,
    email: field => `The ${field} field must be a valid email.`,
    includes: field => `The selected ${field} option is invalid.`,
    integer: field => `The ${field} field must be an integer.`,
    ipAddress: field => `The ${field} field must be a valid ip address.`,
    maxAddress: field => `The ${field} field must be a valid mac address.`,
    maxLength: (field, { max }) => `The ${field} field must not be greater than ${max} characters.`,
    maxValue: (field, { max }) => `The ${field} field must be ${max} or less.`,
    minLength: (field, { min }) => `The ${field} field must be at least ${min} characters.`,
    minValue: (field, { min }) => `The ${field} field must be ${min} or more.`,
    numeric: field => `The ${field} field must contain numeric characters.`,
    required: field => `The ${field} field is required.`,
    sameAs: (field, { eq }) => `The ${field} field must match the ${eq} field.`,
    size: (field, { max, size }) => `The ${field} field may not be greater than ${max} ${size}.`,
    tel: (field, { format }) => `The ${field} field must be a valid phone number${format
        ? `: ${format}`
        : '.'}`,
    url: field => `The ${field} field must be a valid URL.`,
};

export default messages;
